
import {fetch} from 'whatwg-fetch'

function checkStatus(response) {
    if (response.status >= 200 && response.status < 300) {
      return response
    } else {
      var error = new Error(response.statusText)
      error.response = response
      throw error
    }
  }
  function parseJSON(response) {
    return response.json()
  }

export const api = {
    wordcount: (data) => fetch('api/wordcount', {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
          },
        body: JSON.stringify({url: data})
        })
        .then(checkStatus)
        .then(parseJSON)
        .then( (data) => data )
        .catch( (error) => error)
            
        
}