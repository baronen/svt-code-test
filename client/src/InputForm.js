import React from 'react';
import { api } from './api/api';

export class InputForm extends React.Component {
    state = {
        value: ''
    }
    handleSubmit = (event) => {
        if (this.state.value === '' || this.state.value === null) return null
        api.wordcount(this.state.value).then(
            response => this.props.callback(response)
        )
        event.preventDefault();
    }
    handleChange = (event) => this.setState({ value: event.target.value })

    render() {
        return (
            <div style={{ display: 'flex', justifyContent:'center' }}>
                <form onSubmit={this.handleSubmit}>
                    <input type="text" placeholder='Rsslänk eller hashtag' value={this.state.value} onChange={this.handleChange} />
                    <input type="submit" value="Submit" />
                </form>
            </div>
        )
    }
}

