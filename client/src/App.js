import React, { Component } from 'react';
import { InputForm } from './InputForm'
import { Wordcloud } from './Wordcloud'
import './style.css';

const Container = props => (
  <div style={{ flex:'auto', margin: '1rem', flexDirection: 'column', alignItems: 'middle', justifyContent: 'center' }}>
    {props.children}
  </div>
)

const Header = props => (
  <div style={{ display: 'flex', backgroundColor: 'steelblue', justifyContent: 'center' }}>
    <p>
      Tag Cloud by {props.firstName} {props.lastName}
    </p>
  </div>
)

const Footer = props => (
  <div style={{ width: '100%', backgroundColor: 'steelblue' }}>
    <div style={{ padding: '1rem', textAlign: 'center' }}>
      <a href={props.email}>{props.email}</a>

    </div>
  </div>
)

class App extends Component {
  state = {
    words: null,
  }
  handleReceivedData = data => this.setState({
    words: data
  })
  clearReceivedData = () => this.setState({
    words: null
  })
  render() {
    return (
      <div style={{ display: 'flex', flexDirection: 'column', minHeight: '100vh' }}>
        <Header firstName='Carl' lastName='Sagan' />
        <Container>
          <InputForm callback={this.handleReceivedData} />
          <Wordcloud callback={this.clearReceivedData} words={this.state.words} />
        </Container>
        <Footer email='recruitment@damm.io' />
      </div >

    );
  }
}

export default App;
