import React from 'react';

export class Wordcloud extends React.Component {
    render() {
        if (!this.props.words) return null
        const { words } = this.props
        const wordCloudFrequencies = words.map(word => word.frequency)
        const maxSize = Math.max(...wordCloudFrequencies)
        return (
            <div style={{ justifyContent: 'center', display: 'flex', margin: 'auto', flexWrap: 'wrap', flexBasis: 'auto', alignItems: 'center' }}>
                {words.map((word, index) => {
                    const wordPronounciation = (word.frequency / maxSize) * 100 + 8
                    return <p key={index} style={{ display: 'block', margin: '.5rem ', fontSize: wordPronounciation + 'px' }} >
                        {word.word}
                    </p>
                })}
            </div>

        )
    }
}

