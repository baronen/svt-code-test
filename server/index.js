const fetch = require('./lib/fetch/fetch')
const service = require('restana')({
    ignoreTrailingSlash: true
});
const bodyParser = require('body-parser')
service.use(bodyParser.json())

service.post('/api/wordcount', async (req, res) => {
    res.send(await fetch(req.body.url));
});
service.start(8080)