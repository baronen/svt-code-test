const sw = require('stopword');
const alphabeticalOnly = RegExp('^[A-Z]+$', 'gi')
const wordprocessing = {
    findAndGroupOccurances: (content) => {
        const string = typeof content === 'string' ? content.split(' ') : content.toString().split(' ')
        const stripedStopWords = wordprocessing.group(wordprocessing.stripStopwords(string)) // list of words
        const totalList =  wordprocessing.convertToList(stripedStopWords)
        const scrubedList = wordprocessing.scrubNumbersAndTags(totalList)
        return wordprocessing.limitToHundred(scrubedList)
    },
    group: (array) => (
        array.sort()
            .reduce((accumulator, current) => {
                const trimed = current.trim().replace(/[.|,\s]/g, '').toLowerCase()
                accumulator[trimed] = (accumulator[trimed] || 0) + 1;
                return accumulator;
            }, {})
    ),
    stripStopwords: (string) => (
        sw.removeStopwords(string)
    ),
    scrubNumbersAndTags: (list) => {
        return list.filter(f => alphabeticalOnly.test(f.word))
    },
    convertToList: (object) => {
        return Array.from(Object.entries(object)).map(([word, frequency]) => ({ word, frequency }));
    },
    limitToHundred: (list) => (
        list.slice(0, 100)
    )
}

module.exports = wordprocessing