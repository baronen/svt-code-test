const wordprocessing = require('./wordProcessing')
const hugeSample = require('../../mocks/rssResponseMocks')
describe('wordprocessing', () => {
    const testSample=[
        'foo foo bar baz'
    ]
    it('should find similar words', () => {
        const string = typeof testSample === 'string' ? testSample.split(' ') : testSample.toString().split(' ')
        const output = wordprocessing.group(string)
        expect(output.foo).toBe(2)
        expect(output.bar).toBe(1)
        expect(output.baz).toBe(1)
    })
    it('should remove stopwords', () => {
        const stopwords = ['the a foo ']
        const testSamlpleWithStopWords = stopwords[0].concat(testSample[0]).split(' ');
        expect(wordprocessing.stripStopwords(testSamlpleWithStopWords).length).toBe(5)
    })
    it( 'should only return the top 100 hits', () => {
        expect(Object.keys(wordprocessing.findAndGroupOccurances(hugeSample.nasa)).length).toBe(100)
    })
})
