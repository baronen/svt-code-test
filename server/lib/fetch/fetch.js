const wordProcessing = require('../wordProcessing/wordProcessing')
const rss = require('../rss/rss')
const twitter = require('../twitter/twitter')



const rssFetch = async (query) => {
    if (query === '' || query === null || query === undefined) throw new Error(`query field can't be empty`)
    if (query.charAt(0) === '#') {
        return wordProcessing.findAndGroupOccurances(await twitter.parseAsync(query))
    }
    else {
        if (!rss.validateQuery(query)) throw new Error('must be a valid url');
        return wordProcessing.findAndGroupOccurances(await rss.parseAsync(query))
    }
}

module.exports = rssFetch