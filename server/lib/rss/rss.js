let Parser = require('rss-parser');
let parser = new Parser();
const validateUrlRegex = /^(?:http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/g
const rss = {
  validateQuery: (input) => {
    if (typeof input !== 'string' || !input.match(validateUrlRegex)) return false;
    return true
  },
  extractContentFromRss: (rss) => (
    rss.items.map( item => item.content)
  ),
  parseAsync: async (url) => {
    let feed = await parser.parseURL(url);
    return rss.extractContentFromRss(feed)
  },
};

module.exports = rss