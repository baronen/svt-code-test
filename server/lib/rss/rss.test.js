const rss = require('./rss.js');

describe('rss', () => {
    describe('validate url', () => {
        const invalidUrls = [
            'http://',
            'http://.',
            'http://..',
            'http://../',
            'http://?',
            'http://??',
            'http://??/',
            'http://#',
            'http://##',
            'http://##/',
            '//',
            '//a',
            '///a',
            '///',
            'http:///a',
            'rdar://1234',
            'h://test',
            ':// should fail',
            'ftps://foo.bar/',
            'ftp://foo.bar/',
            'http://-error-.invalid/',
            'http://0.0.0.0',
            'http://10.1.1.0',
            'http://10.1.1.255',
            'http://224.1.1.1',
            'http://1.1.1.1.1',
            'http://123.123.123',
            'http://3628126748',
            'http://10.1.1.1'
        ]

        it('should return null if input is not a string', () => {
            expect(rss.validateUrl(1)).toBe(false);
        });
        it('should return true if input is a string and valid url', () => {
            expect(rss.validateUrl('http://www.google.se')).toBe(true)
            expect(rss.validateUrl('https://www.google.se')).toBe(true)
            expect(rss.validateUrl('www.google.se')).toBe(true)
            expect(rss.validateUrl('google.se')).toBe(true)
        })
        invalidUrls.forEach(url => it(`it should fail on ${url}`, () => {
            expect(rss.validateUrl(url)).toBe(false)
        }))

    })
})